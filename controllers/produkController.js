const { produk, kategori_produk, admin,transaksi } = require("../models"); // Import all models


class ProdukController {
    // Get all
    async getAll(req, res) {
      try {
        let data = await produk.findAll({
        });
        return res.status(201).json({
          message: "Success",
          data,
        });
      } catch (e) {
          console.log(e);
        return res.status(500).json({
          message: "Internal Server Error",
          error: e.message,
        });
      }
    }
  
    // Create Produk
     
    async create(req, res) {
      try {
        console.log(req.body);
        const createdData = await produk.create({
            nama_produk: req.body.nama_produk,
            deskripsi_produk:req.body.deskripsi_produk,
            kategori_id:req.body.kategori_id,
            stok_produk:req.body.stok,
            gambar_produk:  req.body.image ? req.body.image : null,
        });
  
        let data = await produk.findOne({
          where: {
            id: createdData.id,
          },
          include: [
            // join
            { model: kategori_produk, attributes: [["nama_kategori_produk","kategori"]] },
          ],
        });
  
        return res.status(201).json({
          message: "Success",
          data,
        });
      } catch (e) {
        console.log(e);
        return res.status(500).json({
          message: "Internal Server Error",
          error: e.message,
        });
      }
    }

    async update(req, res) {
      const {nama,deskripsi,kategori_id,stok}=req.body;
    try {
      var obj={};
      if(nama)obj.nama_produk=nama;
      if(deskripsi)obj.deskripsi_produk=deskripsi;
      if(kategori_id)obj.kategori_id=kategori_id;
      if(stok)obj.stok_produk=stok;
      if(req.body.image)obj.gambar_produk=req.body.image;
      let updateData = await produk.update({
          ...obj
      },{where:{id:req.params.id}});

      let data = await produk.findOne({
        where: {
          id:req.params.id,
        },
        include: [
          // join
          { model: kategori_produk, attributes: [["nama_kategori_produk","kategori"]] },
        ],
      });

      return res.status(201).json({
        message: "Success",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e.message,
      });
    }
  }
  async delete(req, res) {
  try {
    let deleteData = await produk.destroy({where:{id:req.params.id}});

    return res.status(201).json({
      message: "Success",
      
    });
  } catch (e) {
    return res.status(500).json({
      message: "Internal Server Error",
      error: e.message,
    });
  }
}

  }
  
  module.exports = new ProdukController();
  