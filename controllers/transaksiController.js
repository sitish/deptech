const {
    produk,
    kategori_produk,
    admin,
    transaksi
} = require("../models"); // Import all models
const db = require('../models/index');
class TransaksiController {
    // Get all
    async getAll(req, res) {
        try {
            let data = await transaksi.findAll({});
            return res.status(201).json({
                message: "Success",
                data,
            });
        } catch (e) {
            console.log(e);
            return res.status(500).json({
                message: "Internal Server Error",
                error: e.message,
            });
        }
    }

    async getHistory(req, res) {
        try {
            let obj = {};
            if (req.body.admin_id) obj.admin_id = req.body.admin_id;
            if (req.body.produk_id) obj.produk_id = req.body.produk_id;
            let data = await transaksi.findAll({
                where: obj
            });
            return res.status(201).json({
                message: "Success",
                data,
            });
        } catch (e) {
            console.log(e);
            return res.status(500).json({
                message: "Internal Server Error",
                error: e.message,
            });
        }
    }

    // Create Produk

    async create(req, res) {
        try {
            let createdData = await transaksi.create(req.body);

            let data = await transaksi.findOne({
                where: {
                    id: createdData.id,
                },
            });

            return res.status(201).json({
                message: "Success",
                data,
            });
        } catch (e) {
            return res.status(500).json({
                message: "Internal Server Error",
                error: e.message,
            });
        }
    }

    async update(req, res) {
        try {
            let updateData = await transaksi.create(req.body, {
                where: {
                    id: req.params.id
                }
            });

            let data = await transaksi.findOne({
                where: {
                    id: updateData.id,
                },
            });

            return res.status(201).json({
                message: "Success",
                data,
            });
        } catch (e) {
            return res.status(500).json({
                message: "Internal Server Error",
                error: e.message,
            });
        }
    }
    async delete(req, res) {
        try {
            await transaksi.destroy({
                where: {
                    id: req.params.id
                }
            });
            return res.status(201).json({
                message: "Success",
            });
        } catch (e) {
            return res.status(500).json({
                message: "Internal Server Error",
                error: e.message,
            });
        }
    }

    async stock_in(req, res) {
        const t = await db.sequelize.transaction();
        try {
            const dataProduk = await produk.findOne({
                where: {
                    id: req.body.produk_id
                }
            }, {
                transaction: t
            });
            req.body.admin_id= req.user.id
            console.log(req.user);
           const createData= await transaksi.create({
               admin_id:req.user.id,
               jumlah:parseInt(req.body.jumlah),
               produk_id:req.body.produk_id,
               is_in:true
           }, {
                transaction: t
            });
            await produk.increment('stok_produk' ,{by:req.body.jumlah,
                where: {
                    id: req.body.produk_id
                }
            }, {
                transaction: t
            });
            await t.commit();
            return res.status(201).json({
                message: "Success",
                data:createData
            });
        } catch (e) {
            await t.rollback();
            return res.status(500).json({
                message: "Internal Server Error",
                error: e.message,
            });
        }
    }
    async stock_out(req, res) {
        const t = await db.sequelize.transaction();
        try {
            const dataProduk = await produk.findOne({
                where: {
                    id: req.body.produk_id
                }
            }, {
                transaction: t
            });
            req.body.admin_id= req.user.id
           const createData= await transaksi.create(req.body, {
                transaction: t
            });
            await produk.decrement('stok_produk' ,{by:req.body.jumlah,
                where: {
                    id: req.body.produk_id
                }
            }, {
                transaction: t
            });
            await t.commit();
            return res.status(201).json({
                message: "Success",
                data:createData
            });
        } catch (e) {
            await t.rollback();
            return res.status(500).json({
                message: "Internal Server Error",
                error: e.message,
            });
        }
    }
}

module.exports = new TransaksiController();