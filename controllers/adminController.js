const { produk, kategori_produk, admin,transaksi } = require("../models"); // Import all models


class AdminController {
    // Get all
    async getAll(req, res) {
      try {
        let data = await admin.findAll({});
        return res.status(201).json({
          message: "Success",
          data:data,
        });
      } catch (e) {
          console.log(e);
        return res.status(500).json({
          message: "Internal Server Error",
          error: e.message,
        });
      }
    }
    //get one
    async getOne(req, res) {
      try {
        let data = await admin.findOne({where:{id:req.user.id}
        });
        return res.status(201).json({
          message: "Success",
          data:data,
        });
      } catch (e) {
          console.log(e);
        return res.status(500).json({
          message: "Internal Server Error",
          error: e.message,
        });
      }
    }
  
    // Update Admin  
    async update(req, res) {
        const {nama_depan,nama_belakang,email,tanggal_lahir,jenis_kelamin}=req.body
      try {
        let obj={};
        if(nama_depan)obj.nama_depan=nama_depan;
        if(nama_belakang)obj.nama_belakang=nama_belakang;
        if(email)obj.email=email;
        if(tanggal_lahir)obj.tanggal_lahir=tanggal_lahir;
        if(jenis_kelamin)obj.jenis_kelamin=jenis_kelamin;
        let updatedata = await admin.update({
            ...obj
        },{where:{id:req.user.id}});
  
        let data = await admin.findOne({
          where: {
            id: req.user.id,
          }
        });
  
        return res.status(201).json({
          message: "Success",
          data:data,
        });
      } catch (e) {
        return res.status(500).json({
          message: "Internal Server Error",
          error: e.message,
        });
      }
    }

    // Delete Admin  
    async delete(req, res) {
      try {
        let data = await admin.destroy({
          where: {
            id: req.user.id,
          }
        });
  
        return res.status(201).json({
          message: "Success",
          data:data,
        });
      } catch (e) {
        return res.status(500).json({
          message: "Internal Server Error",
          error: e.message,
        });
      }
    }
  }
  
  module.exports = new AdminController();
  