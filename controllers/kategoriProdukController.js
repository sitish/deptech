const { produk, kategori_produk, admin,transaksi } = require("../models"); // Import all models


class KategoriProdukController {
    // Get all
    async getAll(req, res) {
      try {
        let data = await kategori_produk.findAll({});
        return res.status(201).json({
          message: "Success",
          data,
        });
      } catch (e) {
          console.log(e);
        return res.status(500).json({
          message: "Internal Server Error",
          error: e.message,
        });
      }
    }
  
    // Create Kategoriproduk 
    async create(req, res) {

      try {
          console.log(req.body);
        let createdData = await kategori_produk.create(req.body);
  
        let data = await kategori_produk.findOne({
          where: {
            id: createdData.id,
          }
        });
  
        return res.status(201).json({
          message: "Success",
          data,
        });
      } catch (e) {
          console.log(e);
        return res.status(500).json({
          message: "Internal Server Error",
          error: e.message,
        });
      }
    }

    async update(req, res) {

      try {
        await kategori_produk.update(req.body,{where:{id:req.body.id}});
  
        let data = await kategori_produk.findOne({
          where: {
            id:req.body.id.id,
          }
        });
  
        return res.status(201).json({
          message: "Success",
          data,
        });
      } catch (e) {
          console.log(e);
        return res.status(500).json({
          message: "Internal Server Error",
          error: e.message,
        });
      }
    }

    async delete(req, res) {

      try {
        await kategori_produk.destroy({where:{id:req.body.id}});
    
        return res.status(201).json({
          message: "Success",
        });
      } catch (e) {
          console.log(e);
        return res.status(500).json({
          message: "Internal Server Error",
          error: e.message,
        });
      }
    }
  }
  
  module.exports = new KategoriProdukController();
  