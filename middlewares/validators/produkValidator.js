const { produk, kategori_produk, admin,transaksi } = require("../../models"); // Import all models
const validator = require("validator");

module.exports.create = async (req, res, next) => {
  try {
    // Find Kategori
    let findKategori = await kategori_produk.findOne({
      where: {
        id: req.body.kategori_id,
      },
    });

    let errors = [];

    // Kategori not found
    if (!findKategori) {
      errors.push("Kategori Not Found");
    }

    // Check stok_produk is number
    if (!validator.isNumeric(req.body.stok)) {
      errors.push("stok_produk must be a number");
    }

    // If errors length > 0, it will make errors message
    if (errors.length > 0) {
      // Because bad request
      return res.status(400).json({
        message: errors.join(", "),
      });
    }

    // It means that will be go to the next middleware
    next();
  } catch (e) {
    console.log(e);
    return res.status(500).json({
      message: "Internal Server Error",
      error: e.message,
    });
  }
};
