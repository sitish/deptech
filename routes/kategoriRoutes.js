const express = require("express"); // Import express
const router = express.Router(); // Make a router



// Import controller
const kategoriController = require("../controllers/kategoriProdukController");

router
  .route("/")
  .get(kategoriController.getAll)
  .post(kategoriController.create);
router
  .route("/:id")
  .put(kategoriController.update)
  .delete(kategoriController.delete);



module.exports = router; // Export router