const express = require("express"); // Import express
const router = express.Router(); // Make a router


// Import auth (middleware)
const auth = require("../middlewares/auth");

// Import controller
const transaksiController = require("../controllers/transaksiController");

router
    .route("/")
    .get(transaksiController.getAll)
    .post(transaksiController.create);
router
    .route("/:id")
    .put(transaksiController.update)
    .delete(transaksiController.delete);
router
    .route("/stock_in")
    .post(auth.admin,transaksiController.stock_in);
router
    .route("/stock_out")
    .post(auth.admin,transaksiController.stock_out);



module.exports = router; // Export router