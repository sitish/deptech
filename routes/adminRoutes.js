const express = require("express"); // Import express
const router = express.Router(); // Make a router

// Import auth (middleware)
const auth = require("../middlewares/auth");

// Import controller
const adminController = require("../controllers/adminController");

router
  .route("/")
  .get(auth.admin,adminController.getOne)
  .put(auth.admin,adminController.update)
  .delete(auth.admin,adminController.delete);



module.exports = router; // Export router