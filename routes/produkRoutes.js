const express = require("express"); // Import express
const router = express.Router(); // Make a router

// Import middlewares
const  {imageUpload}  = require("../middlewares/uploads/imageUpload");
const produkValidator = require("../middlewares/validators/produkValidator");

// Import controller
const produkController = require("../controllers/produkController");

router
  .route("/")
  .get(produkController.getAll)
  .post(imageUpload,produkValidator.create,produkController.create);
  router
  .route("/:id")
  .put(imageUpload,produkController.update)
  .delete(produkController.delete);



module.exports = router; // Export router
