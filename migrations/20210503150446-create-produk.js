'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('produk', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      nama_produk: {
        type: Sequelize.STRING
      },
      deskripsi_produk: {
        type: Sequelize.STRING
      },
      gambar_produk: {
        type: Sequelize.STRING
      },
      kategori_id: {
        type: Sequelize.INTEGER
      },
      stok_produk: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      deletedAt: {
        allowNull: true,
        type: Sequelize.DATE
}

    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('produk');
  }
};