const { admin, kategori_produk, produk, transaksi } = require("../models"); // Import all models

// admin and transaksi relationship
admin.hasMany(transaksi, { foreignKey: "admin_id" });
transaksi.belongsTo(admin, { foreignKey: "admin_id" });

// produk and kategori relationship
kategori_produk.hasMany(produk, { foreignKey: "kategori_id" });
produk.belongsTo(kategori_produk, { foreignKey: "kategori_id" });

// produk and transaksi relationship
produk.hasMany(transaksi, { foreignKey: "produk_id" });
transaksi.belongsTo(produk, { foreignKey: "produk_id" });

