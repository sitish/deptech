'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class kategori_produk extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  kategori_produk.init({
    nama_kategori_produk: DataTypes.STRING,
    deskripsi_kategori_produk: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'kategori_produk',
    paranoid: true,
      timestamps: true,
      freezeTableName: true
  });
  return kategori_produk;
};