'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class transaksi extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  transaksi.init({
    produk_id: DataTypes.INTEGER,
    is_in: DataTypes.BOOLEAN,
    is_out: DataTypes.BOOLEAN,
    jumlah: DataTypes.INTEGER,
    admin_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'transaksi',
    paranoid: true,
      timestamps: true,
      freezeTableName: true
  });
  return transaksi;
};